#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH           100
#define DEFAULT_NUMBER_BISECTIONS 1

#include "partition.h"

int main(int argc, char *argv[])
{
    Node *nodes = NULL;
    Edge *edges = NULL;
    Edge *edgeAccumulator, edge;
    uint anEdge = 0;
    FILE *descriptor;
    char buf[MAX_LINE_LENGTH];
    uint numberNodes, numberNodesA = 0, numberNodesB = 0, numberEdges = 0, numberBisections = DEFAULT_NUMBER_BISECTIONS;
    bool success;
    Partition *partitions = NULL;
    uint numberPartitions = 0;
    long int beginEntries = 0;
    float weight;
    uint i, source = 0, destination = 0;

    /* Parameter check */
    if (argc < 2)
        fprintf(stderr, "Invalid parameter\n");

    /* Non-default number of bisections */
    if (argc >= 3)
        numberBisections = atoi(argv[2]);

    if (numberBisections == 0)
    {
        fprintf(stderr, "Invalid number of bisections.\n");
        return 1;
    }

    descriptor = fopen(argv[1], "r");
    if (descriptor == NULL)
    {
        fprintf(stderr, "Invalid input file \"%s\"\n",argv[1]);
        goto error;
    }

    do
    {
        if (fgets(buf, MAX_LINE_LENGTH, descriptor) == NULL)
        {
            fprintf(stderr, "Failed to retrieve first line of file.\n");
            goto error;
        }
    }
    while (buf[0] == '%');

    if (sscanf(buf, "%u %u %u", &numberNodesA, &numberNodesB, &numberEdges) != 3)
    {
        perror("Failed to read number of nodes or edges");
        goto error;
    }

    numberNodes = numberNodesA > numberNodesB ? numberNodesA : numberNodesB;

    fprintf(stdout, "Partitioning input file \"%s\" [nodes = %u, edges = %u]\n",argv[1], numberNodes, numberEdges);

    nodes = malloc(numberNodes * sizeof(Node));
    if (nodes == NULL)
    {
        fprintf(stderr, "Nodes allocation failure!\n");
        goto error;
    }

    if (fscanf(descriptor, "%u %u %f", &source, &destination, &weight) != 3)
    {
        perror("Failed to read file contents");
        goto error;
    }

    /* Read the file */
    beginEntries = ftell(descriptor);
    do
    {
        /* The array itself is zero based while the MTX file format is 1-based. */
        ++nodes[source - 1].numberEdges;
        ++nodes[destination - 1].numberEdges;
        ++anEdge;

        if (fscanf(descriptor, "%u %u %f", &source, &destination, &weight) != 3 && ferror(descriptor))
        {
            perror("Failed to read file contents");
            goto error;
        }
    }
    while (feof(descriptor) == 0 && anEdge < numberEdges);

    if (anEdge != numberEdges)
    {
        fprintf(stderr, "Incorrect number of entries %u != %u\n", anEdge, numberEdges);
        goto error;
    }

    /* We haven't reached the end of file */
    if (fscanf(descriptor, "%s", &buf[0]) != EOF)
    {
        fprintf(stderr, "Non end-of-file error hapenned: %i\n", ferror(descriptor));
        goto error;
    }

    /* We need to allocate twice the amount of edges so that each node has its own edge in the array */
    edges = malloc(2 * numberEdges * sizeof(Edge));
    if (edges == NULL)
    {
        fprintf(stderr, "Edges allocation failure!\n");
        goto error;
    }

    edgeAccumulator = edges;
    for (i = 0; i < numberNodes; ++i)
    {
        nodes[i].whichPartition = INVALID_PARTITION_INDEX;
        nodes[i].allocateEdge = 0;
        nodes[i].edges = edgeAccumulator;
        edgeAccumulator += nodes[i].numberEdges;
    }

    if (fseek(descriptor, beginEntries, SEEK_SET))
    {
        fprintf(stderr, "Failed to return to beginning of file.\n");
        goto error;
    }

    if (fscanf(descriptor, "%u %u %f", &edge.source, &edge.destination, &edge.weight) != 3)
    {
        perror("Failed to read file contents");
        goto error;
    }

    do
    {
        --edge.source;
        --edge.destination;

        memcpy(nodes[edge.source].edges + nodes[edge.source].allocateEdge, &edge, sizeof(edge));
        ++nodes[edge.source].allocateEdge;

        memcpy(nodes[edge.destination].edges + nodes[edge.destination].allocateEdge, &edge, sizeof(edge));
        ++nodes[edge.destination].allocateEdge;

        if (fscanf(descriptor, "%u %u %f", &edge.source, &edge.destination, &edge.weight) != 3 && ferror(descriptor))
        {
            perror("Failed to read file contents");
            goto error;
        }
    }
    while (feof(descriptor) == 0);

    /* We haven't reached the end of file */
    if (fscanf(descriptor, "%s", &buf[0]) != EOF)
    {
        fprintf(stderr, "Non end-of-file error hapenned: %i\n", ferror(descriptor));
        goto error;
    }

    /* We don't need file anymore */
    fclose(descriptor);
    descriptor = NULL;

    /* Perform the multilevel partitioning algorithm */
    success = multilevel_paritioning(nodes,
                                     numberNodes,
                                     numberBisections,
                                     &partitions,
                                     &numberPartitions);

    if (success == false)
    {
        fprintf(stderr, "Partitioning failure!\n");
        goto error;
    }

    if (partitions == NULL || numberPartitions == 0)
    {
        fprintf(stderr, "Multi-level partitioning didn't return any partitions.\n");
        goto error;
    }

    free(edges);
    free(partitions);

    return 0;

error:
    if (partitions != NULL)
        free(partitions);
    if (edges != NULL)
        free(edges);
    if (descriptor != NULL)
        fclose(descriptor);

    return 1;
}
