/* Standard C Headers */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Unix Headers */
#ifdef PARALLEL_ENABLED
# include <pthread.h>
#endif
#include <time.h>

/* Program Headers */
#include "partition.h"
#include "partition_internal.h"

#ifdef PARALLEL_ENABLED
# include <assert.h>
#endif

/* Traces are enabled */
/*#define TRACE_ENABLE*/

/* Statistics timer indexes */
typedef enum
{
    /* Before partitioning */
    INDEX_START = 0,

    /* Done initializing values */
    INDEX_INITIALIZATION_DONE,

    /* Coarsening is completed */
    INDEX_COARSENING_DONE,

    /* Greedy Graph Growing is completed */
    INDEX_GREEDY_DONE,

    /* Uncoarsening is completed */
    INDEX_UNCOARSENING_DONE,

    /* Number of indexes */
    NUM_INDEXES

} stats_enum;

/* Number of nanoseconds in a second */
#define NSEC_PER_USEC 1000
#define USEC_PER_SEC  1000000

/* Difference between two times, in nanoseconds */
#define DIFF_TIME(BEGIN,END) ((END.tv_nsec - BEGIN.tv_nsec) / NSEC_PER_USEC + \
                              (END.tv_sec  - BEGIN.tv_sec)  * USEC_PER_SEC)

#ifdef PARALLEL_ENABLED

/* Shared data across threads */
typedef struct
{
    Node               *nodes;                               /* Node graph */
    uint                nodeNumber;                          /* Total node count */
    uint                numberBisections;                    /* Desired number of bisections (TODO) */
    Partition         **partitions;                          /* Pointer to resulting partitions */
    uint               *numberPartitions;                    /* Number of resulting partitions */
    struct timespec    *stats;                               /* Timing statistics of partitioning phases */
    Partition          *returnedPartitions;                  /* Returned partition from uncoarsening (two) */
    Partition         **left;                                /* Left partition of resulting bisection */
    uint                sizeLeft;                            /* Size of left partition */
    Weight            (*edgeWeight)[NUMBER_COARSENED_NODES]; /* Edge weights */
    Partition          *coarsenedNodes;                      /* Coarsened nodes */
    pthread_barrier_t  *barrier;                             /* Thread barriers */
    pthread_mutex_t    *locks;                               /* Edge weight table locks */
    pthread_mutex_t    *nodeLocks;                           /* Nodes graph locks */
} params_t;

static params_t g_params =
{
    NULL,  /* nodes */
    0,     /* nodeNumber */
    0,     /* numberBisections */
    NULL,  /* partitions */
    NULL,  /* numberPartitions */
    NULL,  /* stats */
    NULL,  /* returnedPartitions */
    NULL,  /* left */
    0,     /* sizeLeft */
    NULL,  /* edgeWeight */
    NULL,  /* coarsenedNodes */
    NULL,  /* barrier */
    NULL,  /* locks */
    NULL,  /* nodeLocks */
};

void *thread_function(void *args)
#else
bool thread_function(Node             *nodes,
                     uint              nodeNumber,
                     uint              numberBisections,
                     Weight            edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                     struct timespec   stats[NUM_INDEXES],
                     Partition       **partitions,
                     uint             *numberPartitions)
#endif
{
#ifdef PARALLEL_ENABLED
    /* Thread ID */
    const uint threadID = (uint)args;

    /* Perform coarsening */
    if (heavy_edge_matching_coarsening(g_params.nodes,
                                       g_params.nodeNumber,
                                       g_params.coarsenedNodes,
                                       g_params.edgeWeight,
                                       threadID,
                                       g_params.barrier,
                                       g_params.locks,
                                       g_params.nodeLocks) == false)
    {
        fprintf(stderr, "Coarsening failed!\n");
        goto error;
    }

    clock_gettime(CLOCK_REALTIME, g_params.stats + INDEX_COARSENING_DONE);

    /* Perform partitioning */
    if (greedy_graph_growing_partitioning(g_params.nodeNumber,
                                          g_params.coarsenedNodes,
                                          g_params.edgeWeight,
                                          &g_params.left,
                                          &g_params.sizeLeft,
                                          threadID,
                                          g_params.barrier) == false)
    {
        fprintf(stderr, "Greedy Graph Growing Partitioning failed!\n");
        goto error;
    }

    clock_gettime(CLOCK_REALTIME, g_params.stats + INDEX_GREEDY_DONE);

    /* Perform uncoarsening */
    g_params.returnedPartitions = malloc(2 * sizeof(Partition));
    if (uncoarsening(g_params.nodeNumber,
                     g_params.coarsenedNodes,
                     g_params.left,
                     g_params.sizeLeft,
                     g_params.returnedPartitions,
                     threadID,
                     g_params.barrier) == false)
    {
        fprintf(stderr, "Uncoarsening failed!\n");
        goto error;
    }

    clock_gettime(CLOCK_REALTIME, g_params.stats + INDEX_UNCOARSENING_DONE);

    /* TODO: Function should be able to perform partitoning on any number of partitions. */
    (void)g_params.numberBisections;
    if (g_params.numberPartitions != NULL)
        *g_params.numberPartitions = 2;
    if (g_params.partitions != NULL)
        *g_params.partitions = g_params.returnedPartitions;
#else
    /* Declarations */
    Partition   coarsenedNodes[NUMBER_COARSENED_NODES];
    Partition  *returnedPartitions = NULL;
    Partition **left = NULL;
    uint        sizeLeft = 0;

    /* Perform coarsening */
    if (heavy_edge_matching_coarsening(nodes, nodeNumber, coarsenedNodes, edgeWeight) == false)
    {
        fprintf(stderr, "Coarsening failed!\n");
        goto error;
    }

    clock_gettime(CLOCK_REALTIME, stats + INDEX_COARSENING_DONE);

    /* Perform partitioning */
    if (greedy_graph_growing_partitioning(nodeNumber, coarsenedNodes, edgeWeight, &left, &sizeLeft) == false)
    {
        fprintf(stderr, "Greedy Graph Growing Partitioning failed!\n");
        goto error;
    }

    clock_gettime(CLOCK_REALTIME, stats + INDEX_GREEDY_DONE);

    /* Perform uncoarsening */
    returnedPartitions = malloc(2 * sizeof(Partition));
    if (uncoarsening(nodeNumber, coarsenedNodes, left, sizeLeft, returnedPartitions) == false)
    {
        fprintf(stderr, "Uncoarsening failed!\n");
        goto error;
    }

    clock_gettime(CLOCK_REALTIME, stats + INDEX_UNCOARSENING_DONE);

    /* TODO: Function should be able to perform partitoning on any number of partitions. */
    (void)numberBisections;
    if (numberPartitions != NULL)
        *numberPartitions = 2;
    if (partitions != NULL)
        *partitions = returnedPartitions;

#endif

#ifdef TRACE_ENABLE
    do
    {
        uint i;
#ifdef PARALLEL_ENABLED
        Weight (*edgeWeight)[NUMBER_COARSENED_NODES] = g_params.edgeWeight;
#endif
        printf("Coarsened nodes weight table\n\n");
        for (i = 0; i < NUMBER_COARSENED_NODES; ++i)
        {
            uint j;
            for (j = 0; j < NUMBER_COARSENED_NODES; ++j)
                printf("%f[%u] ", edgeWeight[i][j].weight, edgeWeight[i][j].connections);
            printf("\n");
        }
    }
    while (0);
#endif

#ifdef PARALLEL_ENABLED
    pthread_exit((void *)true);
#endif

    return (void *)true;

error:

#ifdef PARALLEL_ENABLED
    if (g_params.left)
        free(g_params.left);

    if (g_params.returnedPartitions)
        free(g_params.returnedPartitions);

    pthread_exit(false);
#else
    if (left)
        free(left);

    if (returnedPartitions)
        free(returnedPartitions);
#endif

    return false;
}

bool multilevel_paritioning(Node       *nodes,
                            uint        nodeNumber,
                            uint        numberBisections,
                            Partition **partitions,
                            uint       *numberPartitions)
{
    unsigned long   diff;
    bool            success = false;
    struct timespec stats[NUM_INDEXES];
    float           totalWeight = 0.0f;
    unsigned long   totalConnections = 0;
    uint            i;
#ifdef PARALLEL_ENABLED
    pthread_t           threads[NUMBER_THREADS];
    pthread_barrier_t   barrier;
#ifndef NDEBUG
    int                 rc;
#endif
    void              **bool_ptr;
    Weight              edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES];
    Partition           coarsenedNodes[NUMBER_COARSENED_NODES];
    pthread_mutex_t     locks[NUMBER_EDGE_WEIGHT_LOCKS];
    pthread_mutex_t     nodeLocks[NUMBER_NODE_LOCKS];
#else
    Weight edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES];
#endif

    /* Start time */
    clock_gettime(CLOCK_REALTIME, stats + INDEX_START);

#ifdef PARALLEL_ENABLED

    /* Create the barrier */
    if (pthread_barrier_init(&barrier, NULL, NUMBER_THREADS) != 0)
    {
        fprintf(stderr, "Error initializing barriers!\n");
        goto error;
    }

    /* Create the locks */
    i = 0;
    do
    {
        if (pthread_mutex_init(&locks[i], NULL) != 0)
        {
            fprintf(stderr, "Mutex initialization failure!\n");
            goto error;
        }
    }
    while (++i < NUMBER_EDGE_WEIGHT_LOCKS);

    i = 0;
    do
    {
        if (pthread_mutex_init(&nodeLocks[i], NULL) != 0)
        {
            fprintf(stderr, "Mutex initialization failure!\n");
            goto error;
        }
    }
    while (++i < NUMBER_NODE_LOCKS);

    /* Initialization of state table */
    g_params.barrier = &barrier;
    g_params.nodes = nodes;
    g_params.nodeNumber = nodeNumber;
    g_params.numberBisections = numberBisections;
    g_params.partitions = partitions;
    g_params.numberPartitions = numberPartitions;
    g_params.stats = stats;
    g_params.edgeWeight = edgeWeight;
    g_params.coarsenedNodes = coarsenedNodes;
    g_params.locks = locks;
    g_params.nodeLocks = nodeLocks;

    i = 0;

    /* Initialization done */
    clock_gettime(CLOCK_REALTIME, stats + INDEX_INITIALIZATION_DONE);

    /* Create threads */
    do
    {
#ifdef NDEBUG
        pthread_create(&threads[i], NULL, thread_function, (void *)i);
#else
        rc = pthread_create(&threads[i], NULL, thread_function, (void *)i);
#endif

        /* Thread creation should work */
        assert(rc == 0);
    }
    while (++i < NUMBER_THREADS);

    /* Wait for all threads to complete */
    bool_ptr = (void **)&success;

#ifdef NDEBUG
    pthread_join(threads[0], bool_ptr);
#else
    rc = pthread_join(threads[0], bool_ptr);
#endif

    assert(rc == 0);
    for (i = 1; i < NUMBER_THREADS; ++i)
    {
#ifdef NDEBUG
        pthread_join(threads[i], NULL);
#else
        rc = pthread_join(threads[i], NULL);
#endif
        assert(rc == 0);
    }

    /* Destroy mutexes */
    i = 0;
    g_params.locks = NULL;
    do
    {
        if (pthread_mutex_destroy(&locks[i]) != 0)
        {
            fprintf(stderr, "Failed to destroy mutex!\n");
            goto error;
        }
    }
    while (++i < NUMBER_EDGE_WEIGHT_LOCKS);

    i = 0;
    g_params.nodeLocks = NULL;
    do
    {
        if (pthread_mutex_destroy(&nodeLocks[i]) != 0)
        {
            fprintf(stderr, "Failed to destroy mutex!\n");
            goto error;
        }
    }
    while (++i < NUMBER_NODE_LOCKS);

    /* Destroy barrier */
    if (pthread_barrier_destroy(&barrier) != 0)
    {
        fprintf(stderr, "Failed to destroy the barrier.\n");
        g_params.barrier = NULL;
        goto error;
    }

#else
    /* Initialization done */
    clock_gettime(CLOCK_REALTIME, stats + INDEX_INITIALIZATION_DONE);

    /* Call the same thread function, but sequentially */
    success = thread_function(nodes,
                              nodeNumber,
                              numberBisections,
                              edgeWeight,
                              stats,
                              partitions,
                              numberPartitions);
#endif

    /* Calculate statistics */
    i = 0;
    do
    {
        uint j = 0;
        do
        {
            totalWeight += edgeWeight[i][j].weight;
            totalConnections += edgeWeight[i][j].connections;
        }
        while (++j < NUMBER_COARSENED_NODES);
    }
    while (++i < NUMBER_COARSENED_NODES);

    /* Print statistics */
    printf("\n----------- Results ----------\n");
    printf("Total Connection  %12lu\n", totalConnections);
    printf("Total Weight    %14.2f\n\n", totalWeight);
    printf("----------- Timings ----------\n");
    diff = DIFF_TIME(stats[INDEX_START], stats[INDEX_INITIALIZATION_DONE]);
    printf("Initialization %12lu us\n", diff);
    diff = DIFF_TIME(stats[INDEX_INITIALIZATION_DONE], stats[INDEX_COARSENING_DONE]);
    printf("Coarsening     %12lu us\n", diff);
    diff = DIFF_TIME(stats[INDEX_COARSENING_DONE], stats[INDEX_GREEDY_DONE]);
    printf("Partitioning   %12lu us\n", diff);
    diff = DIFF_TIME(stats[INDEX_GREEDY_DONE], stats[INDEX_UNCOARSENING_DONE]);
    printf("Uncoarsening   %12lu us\n", diff);
    diff = DIFF_TIME(stats[INDEX_START], stats[INDEX_UNCOARSENING_DONE]);
    printf("Total          %12lu us\n\n", diff);

    return success;

#ifdef PARALLEL_ENABLED
error:

    if (g_params.locks)
    {
        i = 0;
        do
        {
            pthread_mutex_destroy(&locks[i]);
        }
        while (++i < NUMBER_EDGE_WEIGHT_LOCKS);
    }

    if (g_params.nodeLocks)
    {
        i = 0;
        do
        {
            pthread_mutex_destroy(&nodeLocks[i]);
        }
        while (++i < NUMBER_NODE_LOCKS);
    }

    if (g_params.barrier)
        pthread_barrier_destroy(g_params.barrier);

    return false;
#endif
}
