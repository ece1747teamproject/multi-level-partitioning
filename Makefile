CC=gcc
CFLAGS_BASE=-Wall -Wextra -Werror -pedantic-errors -lpthread
CFLAGS=-O3 -DNDEBUG ${CFLAGS_BASE}
CFLAGS_DEBUG=-g ${CFLAGS_BASE}
LDFLAGS_BASE=-Wall -Wextra -Werror -pedantic-errors -lrt -lpthread
LDFLAGS=-O3 ${LDFLAGS_BASE}
LDFLAGS_DEBUG=-g ${LDFLAGS_BASE}
OBJECTS=release/coarsening.o release/greedy_partitioning.o release/main.o release/partition.o release/uncoarsening.o
OBJECTS_DEBUG=debug/coarsening.o debug/greedy_partitioning.o debug/main.o debug/partition.o debug/uncoarsening.o
EXECUTABLE=./benchmark
EXECUTABLE_DEBUG=./benchmark_debug
BENCHMARK=./inputs/mark3jac140.mtx

all: $(SOURCES) $(EXECUTABLE)

# For C source files
release/%.o: %.c partition.h partition_internal.h
	@mkdir -p release
	${CC} ${CFLAGS} -o $@ -c $<

debug/%.o: %.c partition.h partition_internal.h
	@mkdir -p debug
	${CC} ${CFLAGS_DEBUG} -o $@ -c $<

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

$(EXECUTABLE_DEBUG): $(OBJECTS_DEBUG)
	$(CC) $(LDFLAGS_DEBUG) $(OBJECTS_DEBUG) -o $@

run: $(EXECUTABLE)
	@$(EXECUTABLE) $(BENCHMARK) 2

run_debug: $(EXECUTABLE_DEBUG)
	@$(EXECUTABLE_DEBUG) $(BENCHMARK) 2

run_gdb: run_debug
	gdb --args $(EXECUTABLE_DEBUG) $(BENCHMARK) 2

debug: ${EXECUTABLE_DEBUG}

release: ${EXECUTABLE}

default: release

clean:
	@rm -f ${EXECUTABLE} ${EXECUTABLE_DEBUG} ${OBJECTS} ${OBJECTS_DEBUG}

.PHONY: all clean
