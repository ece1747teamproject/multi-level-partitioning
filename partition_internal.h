#ifndef _PARTITION_INTERNAL_H_
# define _PARTITION_INTERNAL_H_

/* Parallel Partitioning is enabled */
# define PARALLEL_ENABLED

/* Standard C Headers */
# include <stdbool.h>

/* Unix Headers */
# ifdef PARALLEL_ENABLED
#  include <pthread.h>
# endif

/* Program Headers */
# include "partition.h"

/* Size of coarsened nodes during the coarsening phase.  Should never be zero. */
# define NUMBER_COARSENED_NODES 128

/* Multithreading Options */
# ifdef PARALLEL_ENABLED

/* Number of threads.  Should never be zero. */
#  define NUMBER_THREADS 4

/* Number of locks for edge weight table.  Should never be zero. */
#  define NUMBER_EDGE_WEIGHT_LOCKS 64

/* Number of locks for nodes graph.  Should never be zero. */
#  define NUMBER_NODE_LOCKS 64

#  define BARRIER_CHECK(A)                                                 \
    do                                                                     \
    {                                                                      \
        int rc = pthread_barrier_wait(A);                                  \
        if (rc != PTHREAD_BARRIER_SERIAL_THREAD && rc)                     \
        {                                                                  \
            fprintf(stderr, "Failed to synchronize at barrier: %d\n", rc); \
            goto error;                                                    \
        }                                                                  \
    }                                                                      \
    while (0)

# endif

/* Coarsened nodes edge weights */
typedef struct
{
    /* Weight */
    float weight;

    /* Connections */
    unsigned int connections;

} Weight;

/* Coarsening Phase
 *
 * Note: The caller is responsible for deallocating coarsened node pointers.
 */
#ifdef PARALLEL_ENABLED
bool heavy_edge_matching_coarsening(Node              *nodes,
                                    const uint         nodeNumber,
                                    Partition          coarsenedNodes[NUMBER_COARSENED_NODES],
                                    Weight             edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                                    const uint         threadID,
                                    pthread_barrier_t *barrier,
                                    pthread_mutex_t   *locks,
                                    pthread_mutex_t   *nodeLocks);
#else
bool heavy_edge_matching_coarsening(Node       *nodes,
                                    const uint  nodeNumber,
                                    Partition   coarsenedNodes[NUMBER_COARSENED_NODES],
                                    Weight      edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES]);
#endif

/* Partitioning Phase
 *
 * Note: The caller is responsible for deallocating partitions.
 */
#ifdef PARALLEL_ENABLED
bool greedy_graph_growing_partitioning(const uint           nodeNumber,
                                       Partition            coarsenedNodes[NUMBER_COARSENED_NODES],
                                       Weight               edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                                       Partition         ***left,
                                       uint                *sizePartition,
                                       const uint           threadID,
                                       pthread_barrier_t   *barrier);
#else
bool greedy_graph_growing_partitioning(const uint    nodeNumber,
                                       Partition     coarsenedNodes[NUMBER_COARSENED_NODES],
                                       Weight        edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                                       Partition  ***left,
                                       uint         *sizePartition);
#endif

/* Uncoarsening Phase
 *
 * Note: The caller is responsible for deallocating nodes list in each partition.
 */
#ifdef PARALLEL_ENABLED
bool uncoarsening(const uint          nodeNumber,
                  const Partition     coarsenedNodes[NUMBER_COARSENED_NODES],
                  Partition         **left,
                  const uint          sizeLeft,
                  Partition           partitions[2],
                  const uint          threadID,
                  pthread_barrier_t  *barrier);
#else
bool uncoarsening(const uint        nodeNumber,
                  const Partition   coarsenedNodes[NUMBER_COARSENED_NODES],
                  Partition       **left,
                  const uint        sizeLeft,
                  Partition         partitions[2]);
#endif

#endif /* _PARTITION_INTERNAL_H_ */
