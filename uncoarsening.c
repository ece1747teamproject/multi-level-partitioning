/* Standard C Headers */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/* Program Headers */
#include "partition.h"
#include "partition_internal.h"

/* Unix Headers */
#ifdef PARALLEL_ENABLED
# include <pthread.h>
#endif

/* Uncoarsening reallocation expansion factor. Must be an int */
#define UNCOARSENING_REALLOC_EXPANSION_FACTOR 2

/* Refinement Phase */
static void boundary_greedy_kernighan_lin_refinement(void)
{
}

#ifdef PARALLEL_ENABLED

/* Shared data across threads */
typedef struct
{
    Partition *partitions; /* Resulting partitions */
    pthread_mutex_t lock;  /* Graph lock */
} params_t;

static params_t g_params =
{
    NULL                      /* partitions */,
    PTHREAD_MUTEX_INITIALIZER /* lock */
};

/* Uncoarsening Phase */
bool uncoarsening(const uint          nodeNumber,
                  const Partition     coarsenedNodes[NUMBER_COARSENED_NODES],
                  Partition         **left,
                  const uint          sizeLeft,
                  Partition           partitions[2],
                  const uint          threadID,
                  pthread_barrier_t  *barrier)
#else
bool uncoarsening(const uint        nodeNumber,
                  const Partition   coarsenedNodes[NUMBER_COARSENED_NODES],
                  Partition       **left,
                  const uint        sizeLeft,
                  Partition         partitions[2])
#endif
{
    const size_t     size              = (nodeNumber >> 1) * sizeof(Node *);
    const Partition *aCoarsenedNode;
    const Partition *lastCoarsenedNode;

#ifdef PARALLEL_ENABLED
    aCoarsenedNode = coarsenedNodes + (threadID * NUMBER_COARSENED_NODES) / NUMBER_THREADS;
    if (threadID == NUMBER_THREADS - 1)
        lastCoarsenedNode = coarsenedNodes + NUMBER_COARSENED_NODES;
    else
        lastCoarsenedNode = coarsenedNodes + ((threadID + 1) * NUMBER_COARSENED_NODES) / NUMBER_THREADS;
#else
    aCoarsenedNode = coarsenedNodes;
    lastCoarsenedNode = coarsenedNodes + NUMBER_COARSENED_NODES;
#endif

#ifdef PARALLEL_ENABLED
    if (threadID == 0)
    {
#endif
        partitions[0].nodes = malloc(size);
        if (partitions[0].nodes == NULL)
        {
            fprintf(stderr, "Partition nodes allocation failure.\n");
            goto error;
        }

        partitions[1].nodes = malloc(size);
        if (partitions[1].nodes == NULL)
        {
            fprintf(stderr, "Partition nodes allocation failure.\n");
            goto error;
        }

#ifdef PARALLEL_ENABLED
        g_params.partitions = partitions;

        /* Partition initializations */
        partitions[0].size = size;
        partitions[1].size = size;
        partitions[0].numberNodes = 0;
        partitions[1].numberNodes = 0;
    }

    /* Every threads from the partitioning phase
     * must be completed before uncoarsening.
     */
    BARRIER_CHECK(barrier);
    pthread_mutex_lock(&g_params.lock);
    partitions = g_params.partitions;
    pthread_mutex_unlock(&g_params.lock);

#else
    /* Partition initializations */
    partitions[0].size = size;
    partitions[1].size = size;
    partitions[0].numberNodes = 0;
    partitions[1].numberNodes = 0;
#endif

    do
    {
        Partition  **end        = left + sizeLeft;
        Partition  **left_index = left;
        bool         found      = false;
        Node        *node       = *aCoarsenedNode->nodes;
        const Node  *lastNode   = *aCoarsenedNode->nodes + aCoarsenedNode->numberNodes;
        Partition   *selectedPartition;

        /* Find the coarsened node in the left bisection */
        while (left_index != end)
        {
            if (*left_index == aCoarsenedNode)
            {
                found = true;
                break;
            }
            ++left_index;
        }

        /* If coarsened node was found in left bisection parttition, select the
         * the first partition, else the coarsened is in the right one, select
         * the second partition.
         */
        if (found == true)
            selectedPartition = partitions;
        else
            selectedPartition = partitions + 1;

#ifdef PARALLEL_ENABLED
        pthread_mutex_lock(&g_params.lock);
#endif

        /* Add all the nodes in the coarsenednode to the selected partition. */
        while (node != lastNode)
        {
            selectedPartition->nodes[selectedPartition->numberNodes] = node;
            ++selectedPartition->numberNodes;

            if (selectedPartition->size <= selectedPartition->numberNodes * sizeof(Node *))
            {
                const size_t newSize = UNCOARSENING_REALLOC_EXPANSION_FACTOR * selectedPartition->size;

                selectedPartition->nodes = realloc(selectedPartition->nodes, newSize);
                if (selectedPartition->nodes == NULL)
                {
                    fprintf(stderr, "Coarsened nodes reallocation failure!\n");
#ifdef PARALLEL_ENABLED
                    pthread_mutex_unlock(&g_params.lock);
#endif
                    goto error;
                }

                selectedPartition->size = newSize;
            }

            ++node;
        }

#ifdef PARALLEL_ENABLED
        pthread_mutex_unlock(&g_params.lock);
#endif
    }
    while (++aCoarsenedNode != lastCoarsenedNode);

    /* Perform the refinement phase */
    boundary_greedy_kernighan_lin_refinement();

    return true;

error:

    if (partitions[0].nodes)
        free(partitions[0].nodes);
    if (partitions[1].nodes)
        free(partitions[1].nodes);

    return false;
}
