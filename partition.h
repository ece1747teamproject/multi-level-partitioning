/*******************************************************************************
 *
 *  Authors:  Jason Deng, Remi Dufour and Mengxi Liao
 *  Date:     December 23rd, 2012
 *
 *  Name:  Multi Level Partitioning
 *
 *  Description: A simple, portable yet efficient version of the Multi-Level
 *               Partitioning algorithm.
 *
 *  Note:  This is public-domain C implementation written from
 *         scratch.  Use it at your own risk.
 *
 ******************************************************************************/
#ifndef _PARTITION_H_
# define _PARTITION_H_

# include <stdbool.h>

/* Node not inside a partition */
#define INVALID_PARTITION_INDEX (unsigned int)-1

/* Unsigned integer */
typedef unsigned int uint;

/* Node Index */
typedef unsigned int Index;

/* Edge */
typedef struct
{
    /* Source node */
    Index source;

    /* Destination node */
    Index destination;

    /* Edge weight */
    float weight;

} Edge;

/* Node */
typedef struct
{
    /* Array of edges */
    Edge *edges;

    /* Number of edges in the array */
    uint numberEdges;

    /* Node is inside which partition */
    uint whichPartition;

    /* INTERNAL: Counter for initializing a single edge array */
    uint allocateEdge;

} Node;

/* Partition */
typedef struct
{
    /* List of nodes */
    Node **nodes;

    /* Number of nodes in the list */
    uint numberNodes;

    /* Size of the node list */
    uint size;

} Partition;

/* Partition functions
 *
 * Note: It is the caller's responsability to free up the allocated partitions
 */
bool multilevel_paritioning(Node       *nodes,
                            uint        nodeNumber,
                            uint        numberBisections,
                            Partition **partitions,
                            uint       *numberPartitions);

#endif /* _PARTITION_H_ */
