#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "partition.h"
#include "partition_internal.h"

/* Unix Headers */
#ifdef PARALLEL_ENABLED
# include <pthread.h>
#endif

/* Coarsening reallocation expansion factor. Must be an int */
#define COARSENING_REALLOC_EXPANSION_FACTOR 2

#ifdef PARALLEL_ENABLED

/* Shared data across threads */
typedef struct
{
    uint            stillCoarsening; /* Number of threads that are still coarsening */
    pthread_mutex_t stillLock;       /* stillCoarsening lock */
} params_t;

static params_t g_params =
{
    NUMBER_THREADS,           /* stillCoarsening */
    PTHREAD_MUTEX_INITIALIZER /* stillLock */
};

/* Update the edges at merging */
static inline void update_edges(const Node      *aNode,
                                const Node      *nodes,
                                Weight           edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                                pthread_mutex_t *locks)
#else
static inline void update_edges(const Node *aNode,
                                const Node *nodes,
                                Weight      edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES])
#endif
{
    /* Declarations */
    Edge       *anEdge   = aNode->edges;
    const Edge *lastEdge = anEdge + aNode->numberEdges;

    /* Nodes should have an edge */
    assert(aNode->numberEdges);

    /* Update the edge weight */
    do
    {
        const Node *other;
#ifdef PARALLEL_ENABLED
        pthread_mutex_t *lock;
#endif

        /* Skip if node is pointing to itself */
        if (anEdge->source == anEdge->destination)
            continue;

        /* Check if node is source */
        if (aNode == nodes + anEdge->source)
        {
            /* Node is source */
            other = nodes + anEdge->destination;

            /* Skip if edge's destination not allocated already */
            if (other->whichPartition == INVALID_PARTITION_INDEX)
                continue;

            /* Skip if destination edge is myself */
            if (other->whichPartition == aNode->whichPartition)
                continue;

            /* Add the weight */
#ifdef PARALLEL_ENABLED
            lock = locks + (aNode->whichPartition * NUMBER_COARSENED_NODES + other->whichPartition) % NUMBER_EDGE_WEIGHT_LOCKS;
            pthread_mutex_lock(lock);
#endif
            edgeWeight[aNode->whichPartition][other->whichPartition].weight += anEdge->weight;
            ++edgeWeight[aNode->whichPartition][other->whichPartition].connections;
#ifdef PARALLEL_ENABLED
            pthread_mutex_unlock(lock);
#endif

            continue;
        }

        /* Node is destination */
        other = nodes + anEdge->source;

        /* Skip if edge's source not allocated already */
        if (other->whichPartition == INVALID_PARTITION_INDEX)
            continue;

        /* Skip if source edge is myself */
        if (other->whichPartition == aNode->whichPartition)
            continue;

        /* Add the weight */
#ifdef PARALLEL_ENABLED
        lock = locks + (other->whichPartition * NUMBER_COARSENED_NODES + aNode->whichPartition) % NUMBER_EDGE_WEIGHT_LOCKS;
        pthread_mutex_lock(lock);
#endif
        edgeWeight[other->whichPartition][aNode->whichPartition].weight += anEdge->weight;
        ++edgeWeight[other->whichPartition][aNode->whichPartition].connections;
#ifdef PARALLEL_ENABLED
        pthread_mutex_unlock(lock);
#endif
    }
    while (++anEdge != lastEdge);
}

/* Coarsening Phase */
#ifdef PARALLEL_ENABLED
bool heavy_edge_matching_coarsening(Node              *nodes,
                                    const uint         nodeNumber,
                                    Partition          coarsenedNodes[NUMBER_COARSENED_NODES],
                                    Weight             edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                                    const uint         threadID,
                                    pthread_barrier_t *barrier,
                                    pthread_mutex_t   *locks,
                                    pthread_mutex_t   *nodeLocks)
#else
bool heavy_edge_matching_coarsening(Node       *nodes,
                                    const uint  nodeNumber,
                                    Partition   coarsenedNodes[NUMBER_COARSENED_NODES],
                                    Weight      edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES])
#endif
{
    bool partitionedOne;

    /* We garantee at least one entry per coarsened nodes, in case we have too few */
    const size_t size = nodeNumber >= NUMBER_COARSENED_NODES ? (nodeNumber / NUMBER_COARSENED_NODES) * sizeof(Node *) : sizeof(Node *);

    /* Initialize the coarsened nodes */
    const    Partition *end;
    register Partition *nodeIndex;
    register uint       i;

    Node *myNode = nodes;
    const Node *nodeEnd = nodes + nodeNumber;

#ifdef PARALLEL_ENABLED
    /* In parallel version, each thread is responsible for initializing
     * different sections of the nodes array and edges table.
     *
     * To prevent buffer underwrite, we bound the forloop to the number
     * of coarsened nodes.  For maximum spread, the multiply should
     * happen before the divide.
     */
    const uint  i_start = (threadID * NUMBER_COARSENED_NODES) / NUMBER_THREADS;
    Partition  *start   = coarsenedNodes + i_start;

    i = i_start;
    nodeIndex = start;
    if (threadID == NUMBER_THREADS - 1)
        end = coarsenedNodes + NUMBER_COARSENED_NODES;
    else
        end = coarsenedNodes + ((threadID + 1) * NUMBER_COARSENED_NODES) / NUMBER_THREADS;
#else
    i = 0;
    nodeIndex = coarsenedNodes;
    end = coarsenedNodes + NUMBER_COARSENED_NODES;
#endif

    do
    {
        register uint j = 0;

        /* Allocating a node array for each coarsened node. */
        nodeIndex->nodes = malloc(size);
        if (nodeIndex->nodes == NULL)
        {
            fprintf(stderr, "Coarsened nodes allocation error!\n");
            goto error;
        }

        /* Edge weight between coarsened nodes initialized to zero */
        do
        {
            edgeWeight[i][j].weight = 0.0f;
            edgeWeight[i][j].connections = 0;
        }
        while (++j < NUMBER_COARSENED_NODES);

        ++i;
    }
    while (++nodeIndex != end);

#ifdef PARALLEL_ENABLED
    BARRIER_CHECK(barrier);
#endif

    /* For each coarsened nodes */
#ifdef PARALLEL_ENABLED
    i = i_start;
    nodeIndex = start;
#else
    i = 0;
    nodeIndex = coarsenedNodes;
#endif
    do
    {
        /* Pick a random node, using a hash function */
        Node *myNode = nodes + i * (nodeNumber / NUMBER_COARSENED_NODES);
        uint j;

        /* Node should be free */
        assert(myNode->whichPartition == INVALID_PARTITION_INDEX);

        /* Add the node already into our partition */
        nodeIndex->nodes[0] = myNode;
        nodeIndex->numberNodes = 1;
        nodeIndex->size = size;

        myNode->whichPartition = i;

        /* Add all cross-partition weight based on node */
        for (j = 0; j < myNode->numberEdges; ++j)
        {
            const Edge *anEdge = myNode->edges + j;
            const Node *otherNode;

            /* Skip if node is pointing to itself */
            if (anEdge->source == anEdge->destination)
                continue;

            /* Check if current node is source */
            if (myNode == nodes + anEdge->source)
            {
                otherNode = nodes + anEdge->destination;
                if (otherNode->whichPartition != INVALID_PARTITION_INDEX)
                {
#ifdef PARALLEL_ENABLED
                    pthread_mutex_t *lock = locks + (i * NUMBER_COARSENED_NODES + otherNode->whichPartition) % NUMBER_EDGE_WEIGHT_LOCKS;
                    pthread_mutex_lock(lock);
#endif
                    edgeWeight[i][otherNode->whichPartition].weight += anEdge->weight;
                    ++edgeWeight[i][otherNode->whichPartition].connections;
#ifdef PARALLEL_ENABLED
                    pthread_mutex_unlock(lock);
#endif
                }

                continue;
            }

            /* Current node is destination */
            otherNode = nodes + anEdge->source;
            if (otherNode->whichPartition != INVALID_PARTITION_INDEX)
            {
#ifdef PARALLEL_ENABLED
                pthread_mutex_t *lock = locks + (otherNode->whichPartition * NUMBER_COARSENED_NODES + i) % NUMBER_EDGE_WEIGHT_LOCKS;
                pthread_mutex_lock(lock);
#endif
                edgeWeight[otherNode->whichPartition][i].weight += anEdge->weight;
                ++edgeWeight[otherNode->whichPartition][i].connections;
#ifdef PARALLEL_ENABLED
                pthread_mutex_unlock(lock);
#endif
            }
        }

        ++i;
    }
    while (++nodeIndex != end);

#ifdef PARALLEL_ENABLED
    BARRIER_CHECK(barrier);
#endif

    /* Traverse every coarsened partition, and sequentially
     * add one node to each until all nodes have been added.
     */
    do
    {
        partitionedOne = false;

        /* For every partition */
#ifdef PARALLEL_ENABLED
        i = i_start;
        nodeIndex = start;
#else
        i = 0;
        nodeIndex = coarsenedNodes;
#endif
        do
        {
#ifdef PARALLEL_ENABLED
            pthread_mutex_t *lock;
#endif
            Node *otherNode;
            Edge *maximumEdgeWeight = NULL;
            float maximumWeight = 0.0f;
            uint j;

            /* For every node contained within that partition */
            for (j = 0; j < nodeIndex->numberNodes; ++j)
            {
                const Node *myNode = *(nodeIndex->nodes + j);
                uint k;

                /* For every edge connected to every node */
                for (k = 0; k < myNode->numberEdges; ++k)
                {
                    Edge *anEdge = myNode->edges + k;
                    float weight;
                    uint l;

                    /* Skip if node is pointing to itself */
                    if (anEdge->source == anEdge->destination)
                        continue;

                    /* Check if current node is source */
                    if (myNode == nodes + anEdge->source)
                    {
                        otherNode = nodes + anEdge->destination;

                        /* Skip if node was already partitioned */
                        if (otherNode->whichPartition != INVALID_PARTITION_INDEX)
                            continue;

#define EVALUATE_WEIGHT                                                 \
    weight = 0.0f;                                                      \
    for (l = 0; l < otherNode->numberEdges; ++l)                        \
    {                                                                   \
        if (nodes[otherNode->edges[l].source].whichPartition == i ||    \
            nodes[otherNode->edges[l].destination].whichPartition == i) \
            weight += otherNode->edges[l].weight;                       \
        else                                                            \
            weight -= otherNode->edges[l].weight;                       \
    }

                        /* Grab if we found a heavier edge. */
                        EVALUATE_WEIGHT

                        /* Grab if we haven't found an initial edge to
                         * merge already or if we found a heavier edge.
                         */
                        if (maximumEdgeWeight == NULL || weight > maximumWeight)
                        {
                            maximumWeight = weight;
                            maximumEdgeWeight = anEdge;
                        }

                        continue;
                    }

                    /* Current node is destination */
                    otherNode = nodes + anEdge->source;
                    if (otherNode->whichPartition != INVALID_PARTITION_INDEX)
                        continue;

                    /* Grab if we found a heavier edge. */
                    EVALUATE_WEIGHT
#undef EVALUATE_WEIGHT
                }
            }

            /* Skip if nothing else is to merge for this coarsened partition */
            if (maximumEdgeWeight == NULL)
            {
                ++i;
                continue;
            }

            assert(nodes[maximumEdgeWeight->source].whichPartition == i ||
                   nodes[maximumEdgeWeight->destination].whichPartition == i);

            /* Merge the node into current coarsened partition */
            partitionedOne = true;

            /* Coarsened node size may need to be increased */
            if (nodeIndex->size <= nodeIndex->numberNodes * sizeof(Node *))
            {
                size_t newSize = COARSENING_REALLOC_EXPANSION_FACTOR * nodeIndex->size;
                nodeIndex->nodes = realloc(nodeIndex->nodes, newSize);
                if (nodeIndex->nodes == NULL)
                {
                    fprintf(stderr, "Coarsened nodes reallocation failure!\n");
                    goto error;
                }
                nodeIndex->size = newSize;
            }

            if (nodes[maximumEdgeWeight->source].whichPartition == i)
            {
                /* We're the source of the edge */
                otherNode = nodes + maximumEdgeWeight->destination;

#ifdef PARALLEL_ENABLED
                /* Lock the graph to ensure that node isn't allocated twice */
                lock = nodeLocks + maximumEdgeWeight->destination % NUMBER_NODE_LOCKS;
                pthread_mutex_lock(lock);
                if (otherNode->whichPartition != INVALID_PARTITION_INDEX)
                {
                    pthread_mutex_unlock(lock);
                    ++i;
                    continue;
                }
#endif
                otherNode->whichPartition = i;

#ifdef PARALLEL_ENABLED
                pthread_mutex_unlock(lock);
#endif
                nodeIndex->nodes[nodeIndex->numberNodes] = otherNode;
                ++nodeIndex->numberNodes;

                /* Update the edge weight */
#ifdef PARALLEL_ENABLED
                update_edges(otherNode, nodes, edgeWeight, locks);
#else
                update_edges(otherNode, nodes, edgeWeight);
#endif

                ++i;
                continue;
            }

            /* We're the destination of the edge */
            otherNode = nodes + maximumEdgeWeight->source;

#ifdef PARALLEL_ENABLED
            /* Lock the graph to ensure that node isn't allocated twice */
            lock = nodeLocks + maximumEdgeWeight->source % NUMBER_NODE_LOCKS;
            pthread_mutex_lock(lock);
            if (otherNode->whichPartition != INVALID_PARTITION_INDEX)
            {
                pthread_mutex_unlock(lock);
                ++i;
                continue;
            }
#endif
            otherNode->whichPartition = i;

#ifdef PARALLEL_ENABLED
            pthread_mutex_unlock(lock);
#endif
            nodeIndex->nodes[nodeIndex->numberNodes] = otherNode;
            ++nodeIndex->numberNodes;

            /* Update the edge weight */
#ifdef PARALLEL_ENABLED
            update_edges(otherNode, nodes, edgeWeight,locks);
#else
            update_edges(otherNode, nodes, edgeWeight);
#endif

            ++i;
        }
        while (++nodeIndex != end);

#ifdef PARALLEL_ENABLED
        /* Thread is done coarsening */
        if (partitionedOne == false)
        {
            pthread_mutex_lock(&g_params.stillLock);
            --g_params.stillCoarsening;
            pthread_mutex_unlock(&g_params.stillLock);
        }

        BARRIER_CHECK(barrier);
#endif
    }
    while (partitionedOne == true);

#ifdef PARALLEL_ENABLED
    /* Loop all threads until everybody is done coarsening */
    pthread_mutex_lock(&g_params.stillLock);
    do
    {
        if (g_params.stillCoarsening == 0)
        {
            pthread_mutex_unlock(&g_params.stillLock);
            break;
        }
        pthread_mutex_unlock(&g_params.stillLock);
        BARRIER_CHECK(barrier);
        pthread_mutex_lock(&g_params.stillLock);
    }
    while (true);

    if (threadID == 0)
    {
#endif

    /* Coarsen unconnected nodes */
    i = 0;
    nodeIndex = coarsenedNodes;
    do
    {
        while (nodeIndex->numberNodes > NUMBER_COARSENED_NODES + nodeNumber / NUMBER_COARSENED_NODES)
        {
            ++nodeIndex;
            ++i;

            /* We should have all nodes distributed equally */
            assert(nodeIndex < coarsenedNodes + NUMBER_COARSENED_NODES);
        }

        /* Coarsened node size may need to be increased */
        if (nodeIndex->size <= nodeIndex->numberNodes * sizeof(Node *))
        {
            size_t newSize = COARSENING_REALLOC_EXPANSION_FACTOR * nodeIndex->size;
            nodeIndex->nodes = realloc(nodeIndex->nodes, newSize);
            if (nodeIndex->nodes == NULL)
            {
                fprintf(stderr, "Coarsened nodes reallocation failure!\n");
                goto error;
            }
            nodeIndex->size = newSize;
        }

        if (myNode->whichPartition == INVALID_PARTITION_INDEX)
        {
            myNode->whichPartition = i;
            nodeIndex->nodes[nodeIndex->numberNodes] = myNode;
            ++nodeIndex->numberNodes;
        }
    }
    while (++myNode < nodeEnd);

#ifdef PARALLEL_ENABLED
    }
#endif

    return true;

error:
    /* Free partition node lists */
    nodeIndex = coarsenedNodes;
    do
    {
        free(nodeIndex->nodes);
    }
    while (++nodeIndex != end);

    return false;
}
