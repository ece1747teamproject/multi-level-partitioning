#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "partition.h"
#include "partition_internal.h"

/* Unix Headers */
#ifdef PARALLEL_ENABLED
# include <pthread.h>
#endif

/* Number of iteration starting points */
#define GREEDY_NUMBER_TRIES 10

#ifdef PARALLEL_ENABLED

/* Shared data across threads */
typedef struct
{
    Partition       **left;                /* Left part of bisection */
    uint              count;               /* Coarsened node count of left part */
    pthread_mutex_t   lock;                /* Candidate lock */
    bool              initialized;         /* Candidate is initialized */
    float             candidateWeight;     /* Candidate total weight count */
    uint              candidateConnection; /* Candidate total connection count */
} params_t;

static params_t g_params =
{
    NULL,                      /* left */
    0,                         /* count */
    PTHREAD_MUTEX_INITIALIZER, /* lock */
    false,                     /* initialized */
    0.0f,                      /* candidateWeight */
    0                          /* candidateConnection */
};

/* Partitioning Phase */
bool greedy_graph_growing_partitioning(const uint           nodeNumber,
                                       Partition            coarsenedNodes[NUMBER_COARSENED_NODES],
                                       Weight               edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                                       Partition         ***left,
                                       uint                *sizePartition,
                                       const uint           threadID,
                                       pthread_barrier_t   *barrier)
#else
bool greedy_graph_growing_partitioning(const uint    nodeNumber,
                                       Partition     coarsenedNodes[NUMBER_COARSENED_NODES],
                                       Weight        edgeWeight[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES],
                                       Partition  ***left,
                                       uint         *sizePartition)
#endif
{
    size_t count = 0;
    Partition **newPartitions;
    uint iteration;
    Partition *(candidatePartition[NUMBER_COARSENED_NODES]);

#ifdef PARALLEL_ENABLED
    uint i_start, i_end;

    /* Thread zero allocates partition pointers */
    if (threadID == 0)
    {
        g_params.left = malloc(NUMBER_COARSENED_NODES * sizeof(Partition *));
        if (g_params.left == NULL)
        {
            fprintf(stderr, "Partition allocation failure.\n");
            goto error;
        }
    }

    /* No thread should start partitioning
     * before the partition table is ready.
     */
    BARRIER_CHECK(barrier);
    newPartitions = g_params.left;

    /* Determine the indexes for each thread.  For maximum
     * spread, the multiply should happen before the divide.
     */
    i_start = (threadID * GREEDY_NUMBER_TRIES) / NUMBER_THREADS;
    if (threadID == NUMBER_THREADS - 1)
        i_end = GREEDY_NUMBER_TRIES;
    else
        i_end = ((threadID + 1) * GREEDY_NUMBER_TRIES) / NUMBER_THREADS;

#else
    float candidateWeight = 0.0f;
    uint candidateConnection = 0;

    newPartitions = malloc(NUMBER_COARSENED_NODES * sizeof(Partition *));
    if (newPartitions == NULL)
    {
        fprintf(stderr, "Partition allocation failure.\n");
        goto error;
    }
#endif

#ifdef PARALLEL_ENABLED
    for (iteration = i_start; iteration < i_end; ++iteration)
#else
    for (iteration = 0; iteration < GREEDY_NUMBER_TRIES; ++iteration)
#endif
    {
        Weight edgeWeightLocal[NUMBER_COARSENED_NODES][NUMBER_COARSENED_NODES];
        uint i, startingPoint, totalEdgeConnection, totalWeight = 0;
        float totalEdgeWeight;

        /* Get a local copy of edge weights */
        memcpy(edgeWeightLocal, edgeWeight, sizeof(edgeWeightLocal));

        /* Determine the starting point */
        startingPoint = (iteration * NUMBER_COARSENED_NODES) / GREEDY_NUMBER_TRIES;

        /* Add the starting point to the bisection already */
        candidatePartition[0] = &coarsenedNodes[startingPoint];
        count = 1;

        /* Add coarsened nodes to the partition until half the partition has been filed */
        do
        {
            float maximumWeight = 0.0f;
            uint candidate = 0, maximumConnection = 0;
            bool initialized = false;

            /* Find the best candidate to merge */
            i = 0;
            do
            {
                uint j;

                /* If node was already merged, skip */
                if (edgeWeightLocal[i][i].connections)
                    continue;

                /* If node corresponds to the starting point, skip */
                if (i == startingPoint)
                    continue;

                /* Pick best candidate fromt tables
                 *
                 * Perform the real GGGP partitioning
                 */

                /* First, determine the gain from merging this candidate */
                totalEdgeWeight = edgeWeightLocal[i][startingPoint].weight + edgeWeightLocal[startingPoint][i].weight;
                totalEdgeConnection = edgeWeightLocal[i][startingPoint].connections + edgeWeightLocal[startingPoint][i].connections;

                /* Then, determine the loss from merging */
                j = 0;
                do
                {
                    /* The starting point is already processed */
                    if (j == startingPoint)
                        continue;

                    totalEdgeWeight -= edgeWeightLocal[i][j].weight + edgeWeightLocal[j][i].weight;
                    totalEdgeConnection -= edgeWeightLocal[i][j].connections + edgeWeightLocal[j][i].connections;
                }
                while (++j < NUMBER_COARSENED_NODES);

                if (initialized == false || totalEdgeWeight > maximumWeight ||
                    (totalEdgeWeight == maximumWeight && totalEdgeConnection > maximumConnection))
                {
                    candidate = i;
                    maximumWeight = totalEdgeWeight;
                    maximumConnection = totalEdgeConnection;
                    initialized = true;
                }
            }
            while (++i < NUMBER_COARSENED_NODES);

            /* Partition array size should be sufficient */
            assert(count < NUMBER_COARSENED_NODES);

            /* Merge the candidate */
            totalWeight += coarsenedNodes[candidate].numberNodes;
            candidatePartition[count] = &coarsenedNodes[candidate];
            ++count;

            /* Update the tables */
            i = 0;
            do
            {
                edgeWeightLocal[startingPoint][i].weight += edgeWeightLocal[candidate][i].weight;
                edgeWeightLocal[startingPoint][i].connections += edgeWeightLocal[candidate][i].connections;
                edgeWeightLocal[i][startingPoint].weight += edgeWeightLocal[i][candidate].weight;
                edgeWeightLocal[i][startingPoint].connections += edgeWeightLocal[i][candidate].connections;

                /* Skip this candidate on the next iteration */
                edgeWeightLocal[candidate][i].weight = 0.0f;
                edgeWeightLocal[candidate][i].connections = 0;
                edgeWeightLocal[i][candidate].weight = 0.0f;
                edgeWeightLocal[i][candidate].connections = 0;
            }
            while (++i < NUMBER_COARSENED_NODES);

            /* Remove starting point's connection to itself */
            edgeWeightLocal[startingPoint][startingPoint].weight = 0.0f;
            edgeWeightLocal[startingPoint][startingPoint].connections = 0;

            /* Mark the candidate as merged */
            edgeWeightLocal[candidate][candidate].connections = -1;
        }
        while (totalWeight * 2 < nodeNumber);

        /* Sum up edge table to see if this is the best candidate */
        totalEdgeWeight = 0.0f;
        totalEdgeConnection = 0;
        i = 0;
        do
        {
            uint j = 0;

            do
            {
                totalEdgeWeight += edgeWeightLocal[i][j].weight;
                totalEdgeConnection += edgeWeightLocal[i][j].connections;
            }
            while (++j < NUMBER_COARSENED_NODES);
        }
        while (++i < NUMBER_COARSENED_NODES);

        /* If this is the best candidate, allocate it to the output */
#ifdef PARALLEL_ENABLED
        pthread_mutex_lock(&g_params.lock);
        if (g_params.initialized == false || totalEdgeWeight > g_params.candidateWeight ||
            (totalEdgeWeight == g_params.candidateWeight && totalEdgeConnection > g_params.candidateConnection))
        {
            g_params.initialized = true;
            g_params.candidateWeight = totalEdgeWeight;
            g_params.candidateConnection = totalEdgeConnection;
            g_params.count = count;
            memcpy(newPartitions, candidatePartition, count * sizeof(Partition *));
        }
        pthread_mutex_unlock(&g_params.lock);
#else
        if (iteration == 0 || totalEdgeWeight > candidateWeight ||
            (totalEdgeWeight == candidateWeight && totalEdgeConnection > candidateConnection))
        {
            candidateWeight = totalEdgeWeight;
            candidateConnection = totalEdgeConnection;
            memcpy(newPartitions, candidatePartition, count * sizeof(Partition *));
        }
#endif
    }

#ifdef PARALLEL_ENABLED
    pthread_mutex_lock(&g_params.lock);
#endif
    *left = newPartitions;
#ifdef PARALLEL_ENABLED
    *sizePartition = g_params.count;
    pthread_mutex_unlock(&g_params.lock);
#else
    *sizePartition = count;
#endif

    return true;

error:

#ifdef PARALLEL_ENABLED
    if (g_params.left)
        free(g_params.left);
#else
    if (newPartitions)
        free(newPartitions);
#endif

    return false;
}
